#!/bin/sh

## Apply templates for dovecot config
echo "* Applying dovecot config"
for i in $(find /etc/dovecot -type f -name *.j2); do
    FILE_IN=${i}
    FILE_OUT=${i%%.j2}

    echo "  - ${FILE_OUT}"
    j2 -o ${FILE_OUT} ${FILE_IN}
done

## Set storage ownership
chown 1001.1001 /srv/mail

## Replace current shell with postfix
exec /usr/sbin/dovecot -c /etc/dovecot/dovecot.conf -F
