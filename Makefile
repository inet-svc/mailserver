all: help

include .env
-include .env.override

help:
	@awk 'BEGIN {FS = ":.*##"; printf "\nUsage:\n  make \033[36m<Subcommand> <Enter>\033[0m (default: help)\n\nSubcommands:\n"} /^[a-zA-Z_-]+:.*?##/ { printf "  \033[36m%-12s\033[0m %s\n", $$1, $$2 }' $(MAKEFILE_LIST)

build-mysql:						## Build MySQL image
	docker build -t $(IMAGE_PREFIX)mysql:$(IMAGE_TAG) mysql

build-dovecot:						## Build Dovecot image
	docker build -t $(IMAGE_PREFIX)dovecot:$(IMAGE_TAG) dovecot

build-postfix:						## Build Postfix image
	docker build -t $(IMAGE_PREFIX)postfix:$(IMAGE_TAG) postfix

build-sabre:						## Build Sabre (WebDAV) image
	docker build -t $(IMAGE_PREFIX)sabre:$(IMAGE_TAG) sabre

build-frontend:						## Build Frontend (webmail, admin, Z-Push) image
	docker build -t $(IMAGE_PREFIX)frontend:$(IMAGE_TAG) frontend

build: build-mysql build-dovecot build-postfix build-sabre build-frontend ## Build all images

test: clean build					## Test run in console
	docker compose --env-file .env --env-file .env.override up || true

start:								## Start the environment (with console detach)
	docker compose --env-file .env --env-file .env.override up -d --wait

stop:								## Stop the environment
	docker compose --env-file .env --env-file .env.override stop

down:								## Destroy the environment (just the containers, data persistence is kept)
	docker compose --env-file .env --env-file .env.override down

clean:								## Cleanup (without destroying data and/or images)
	docker compose --env-file .env --env-file .env.override down
	docker container prune -f
	docker volume prune -f
	docker volume rm -f $(docker volume ls -q -f dangling=true) 2>/dev/null || true

cleandata:
	sudo rm -rf ./data
	docker container prune -f
	docker volume rm -f $(docker volume ls -q -f dangling=true) 2>/dev/null || true

cleanimg:
	docker container prune -f
	docker image prune -af

mrproper: clean cleandata cleanimg	## Full cleanup (removes also all data and runs system prune)
