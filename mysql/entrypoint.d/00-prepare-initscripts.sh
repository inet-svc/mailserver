#!/bin/bash

if [ "$EUID" -ne 0 ]; then
    exit 0;
fi

if [ -f /var/lib/mysql/mysql_upgrade_info ]; then
    exit 0;
fi

## Apply templates for mysql init scripts
echo "* Applying MySQL init script templates"
for i in $(find /docker-entrypoint-initdb.d -type f -name *.j2); do
    FILE_IN=${i}
    FILE_OUT=${i%%.j2}

    echo "  - ${FILE_OUT}"
    j2 -o ${FILE_OUT} ${FILE_IN}
    rm -f ${FILE_IN}
done
