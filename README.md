# mailserver

## Introduction
This is a simple open-source mail server solution composed from well known components, made easy to use.

## Requirements
- [ ] docker engine with docker compose plugin (tested on docker `23.0.5`)
- [ ] `make`

## Usage
Everything needed to spin it up is in `.env` and `compose.yml` files.

- [ ] Edit .env file (or create .env.override) to adjust settings to your environments.
- [ ] Build images with `make build`
- [ ] Run without detach to see if everything works as expected with `make test`
- [ ] Initialize postfixadmin and create admin account at [server]/admin/setup.php
- [ ] Log into postfixadmin at [server]/admin/ and create domains/mailboxes as needed
- [ ] Verify webmail works at [server]
- [ ] Optionally test ActiveSync
- [ ] Optionally verify IMAP/IMAPS access
- [ ] Verify sumission port (587 by default)
- [ ] When everything seems to be OK, just start the service with `make start` and stop with `make stop`
- [ ] To clean docker runtime, run `make clean`
- [ ] To erase everything (system prune + remove all persistence data!!!!) run `make mrproper`
- [ ] Profit
