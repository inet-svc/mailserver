#!/bin/bash

source /usr/lib/postfixadmin/scripts/.config

TIMESTAMP=$(date "+%Y-%m-%d %H:%M:%S")
MAILBOX_NAME=${1}
DOMAIN_NAME=${2}
MAILDIR_PATH=${3}
QUOTA=${4}

### Create CardDAV/CalDAV principal and default calendar/addressbook
cat << EOF | mysql -h ${MYSQL_HOST} -u ${SABRE_MYSQL_USER} --password=${SABRE_MYSQL_PASSWORD} ${SABRE_MYSQL_DATABASE}
INSERT INTO principals (uri, email, displayname) VALUES ("principals/${MAILBOX_NAME}", "${MAILBOX_NAME}", "${MAILBOX_NAME}");
INSERT INTO principals (uri, email, displayname) VALUES ("principals/${MAILBOX_NAME}/calendar-proxy-read", NULL, NULL);
INSERT INTO principals (uri, email, displayname) VALUES ("principals/${MAILBOX_NAME}/calendar-proxy-write", NULL, NULL);

INSERT INTO addressbooks (principaluri, displayname, uri, description, synctoken) VALUES ("principals/${MAILBOX_NAME}", "Default", "default", NULL, 1);

INSERT INTO calendars (synctoken, components) VALUES (1, 'VEVENT,VTODO');
INSERT INTO calendarinstances (calendarid, principaluri, access, displayname, uri, description, calendarorder, calendarcolor, timezone, transparent, share_href, share_displayname, share_invitestatus) VALUES (LAST_INSERT_ID(), "principals/${MAILBOX_NAME}", 1, "Default", "default", NULL, 0, NULL, NULL, 0, NULL, NULL, 2);
EOF
