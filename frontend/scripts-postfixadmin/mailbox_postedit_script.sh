#!/bin/bash

TIMESTAMP=$(date "+%Y-%m-%d %H:%M:%S")
MAILBOX_NAME=${1}
DOMAIN_NAME=${2}
MAILDIR_PATH=${3}
QUOTA=${4}
