<?php
    define('MEMCACHED_SERVERS','localhost:11211');
    define('MEMCACHED_DOWN_LOCK_FILE', '/tmp/z-push-memcache-down');
    define('MEMCACHED_DOWN_LOCK_EXPIRATION', 30);
    define('MEMCACHED_PREFIX', 'z-push-ipc');
    define('MEMCACHED_TIMEOUT', 100);
    define('MEMCACHED_MUTEX_TIMEOUT', 5);
    define('MEMCACHED_BLOCK_WAIT', 10);
?>
