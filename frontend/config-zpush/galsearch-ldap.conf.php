<?php
    define("LDAP_SERVER_URI", "ldap://127.0.0.1:389/");
    define("ANONYMOUS_BIND", true);
    define("LDAP_BIND_USER", "cn=searchuser,dc=test,dc=net");
    define("LDAP_BIND_PASSWORD", "");

    define("LDAP_SEARCH_BASE", "ou=global,dc=test,dc=net");
    define("LDAP_SEARCH_FILTER", "(|(cn=*SEARCHVALUE*)(mail=*SEARCHVALUE*))");

    global $ldap_field_map;
    $ldap_field_map = array(
                    SYNC_GAL_DISPLAYNAME    => 'cn',
                    SYNC_GAL_PHONE          => 'telephonenumber',
                    SYNC_GAL_OFFICE         => '',
                    SYNC_GAL_TITLE          => 'title',
                    SYNC_GAL_COMPANY        => 'ou',
                    SYNC_GAL_ALIAS          => 'uid',
                    SYNC_GAL_FIRSTNAME      => 'givenname',
                    SYNC_GAL_LASTNAME       => 'sn',
                    SYNC_GAL_HOMEPHONE      => 'homephone',
                    SYNC_GAL_MOBILEPHONE    => 'mobile',
                    SYNC_GAL_EMAILADDRESS   => 'mail',
                );


    define("LDAP_SEARCH_NAME_FALLBACK", false);
?>
