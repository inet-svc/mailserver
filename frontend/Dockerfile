FROM alpine:3 as build

ARG VERSION_ZPUSH=2.7.0
ARG VERSION_POSTFIXADMIN=3.3.13
ARG VERSION_ROUNDCUBE=1.6.1

ENV URL_ZPUSH=https://github.com/Z-Hub/Z-Push/archive/refs/tags/${VERSION_ZPUSH}.tar.gz
ENV URL_POSTFIXADMIN=https://github.com/postfixadmin/postfixadmin/archive/postfixadmin-${VERSION_POSTFIXADMIN}.tar.gz
ENV URL_ROUNDCUBE=https://github.com/roundcube/roundcubemail/releases/download/${VERSION_ROUNDCUBE}/roundcubemail-${VERSION_ROUNDCUBE}-complete.tar.gz

ENV SUM_ZPUSH=380f130847333f68e1a26962ede85c12a8ef04bc17f3a5c1054e212f34c020809af503001c8ba3c626e4a049c2034258529019a15e69226238e01182f4baafbd
ENV SUM_POSTFIXADMIN=bf7daaa089ee3adc4b557f1a7d0509d78979ef688fb725bab795f5c9d81e8774296245fde0cb184db51e9185cad381682c3ecc0bfadf852388b499a0a95cca64
ENV SUM_ROUNDCUBE=d5804e91c2da3b732cf14ad86f06099254540dd826ae5b7f14b98a7af8b90a7e9457221da2945aace8d12a60381eec85161e8bc6e0af4572b798a66cb36f2f00

RUN apk --no-cache add patch

RUN mkdir -p /var/tmp/dl /var/tmp/unp/postfixadmin /var/tmp/unp/zpush /var/tmp/unp/roundcube && \
    wget -O /var/tmp/dl/zpush.tar.gz ${URL_ZPUSH} && \
    wget -O /var/tmp/dl/postfixadmin.tar.gz ${URL_POSTFIXADMIN} && \
    wget -O /var/tmp/dl/roundcube.tar.gz ${URL_ROUNDCUBE} && \
    echo "${SUM_ZPUSH} */var/tmp/dl/zpush.tar.gz" | sha512sum -c - && \
    echo "${SUM_POSTFIXADMIN} */var/tmp/dl/postfixadmin.tar.gz" | sha512sum -c - && \
    echo "${SUM_ROUNDCUBE} */var/tmp/dl/roundcube.tar.gz" | sha512sum -c - && \
    tar xvzf /var/tmp/dl/postfixadmin.tar.gz --strip-components=1 -C /var/tmp/unp/postfixadmin && \
    tar xvzf /var/tmp/dl/zpush.tar.gz --strip-components=1 -C /var/tmp/unp/zpush && \
    tar xvzf /var/tmp/dl/roundcube.tar.gz --strip-components=1 -C /var/tmp/unp/roundcube

COPY patches /tmp/patches

RUN (cd /var/tmp/unp/zpush/src/lib/utils/ && patch </tmp/patches/z-push/utils.php.diff)

FROM ubuntu:20.04

ENV container=docker \
    LC_ALL=C
ARG DEBIAN_FRONTEND=noninteractive

RUN apt-get update && apt-get -y --no-install-recommends install \
        python3 python3-pip \
		php7.4-fpm nginx curl \
        php-imap php-mysql php-gd php-mbstring \
        php-xml php-intl php-curl php-ldap \
        php-imagick php-zip libawl-php \
		php-memcached memcached \
        mysql-client && \
    apt-get clean && rm -rf /var/lib/apt/* && \
    rm -f /etc/php/7.4/fpm/pool.d/*.conf

RUN python3 -m pip install --no-cache \
        j2cli

COPY --from=build --chown=root:root /var/tmp/unp/postfixadmin /var/www/postfixadmin
COPY --from=build --chown=root:root /var/tmp/unp/zpush/src /var/www/z-push
COPY --from=build --chown=root:root /var/tmp/unp/roundcube /var/www/roundcube

COPY config-fpm/ /etc/php/7.4/fpm/
COPY config-nginx/ /etc/nginx/

RUN mkdir /var/lib/php-fpm

RUN rm -f /var/www/z-push/config.php \
          /var/www/z-push/policies.ini \
          /var/www/z-push/autodiscover/config.php \
          /var/www/z-push/backend/caldav/config.php \
          /var/www/z-push/backend/carddav/config.php \
          /var/www/z-push/backend/combined/config.php \
          /var/www/z-push/backend/searchldap/config.php \
          /var/www/z-push/backend/imap/config.php \
          /var/www/z-push/backend/ipcmemcached/config.php \
          /var/www/z-push/backend/sqlstatemachine/config.php && \
          ln -s /etc/z-push/z-push.conf.php /var/www/z-push/config.php && \
          ln -s /etc/z-push/policies.ini /var/www/z-push/policies.ini && \
          ln -s /etc/z-push/autodiscover.conf.php /var/www/z-push/autodiscover/config.php && \
          ln -s /etc/z-push/caldav.conf.php /var/www/z-push/backend/caldav/config.php && \
          ln -s /etc/z-push/carddav.conf.php /var/www/z-push/backend/carddav/config.php && \
          ln -s /etc/z-push/combined.conf.php /var/www/z-push/backend/combined/config.php && \
          ln -s /etc/z-push/galsearch-ldap.conf.php /var/www/z-push/backend/searchldap/config.php && \
          ln -s /etc/z-push/imap.conf.php /var/www/z-push/backend/imap/config.php && \
          ln -s /etc/z-push/memcached.conf.php /var/www/z-push/backend/ipcmemcached/config.php && \
          ln -s /etc/z-push/state-sql.conf.php /var/www/z-push/backend/sqlstatemachine/config.php && \
          ln -s /var/www/z-push/z-push-admin.php /usr/bin/z-push-admin && \
          ln -s /var/www/z-push/z-push-top.php /usr/bin/z-push-top

RUN mv /var/www/roundcube/config /etc/roundcube && \
    ln -s /etc/roundcube /var/www/roundcube/config

RUN rm -f /var/www/postfixadmin/config.local.php && \
    ln -s /etc/postfixadmin/config.local.php /var/www/postfixadmin/config.local.php

COPY config-zpush/ /etc/z-push/
COPY config-postfixadmin/ /etc/postfixadmin/
COPY config-roundcube /etc/roundcube

COPY scripts-postfixadmin /usr/lib/postfixadmin/scripts

COPY entrypoint.sh /

VOLUME ["/var/log/z-push"]
HEALTHCHECK --interval=5s --timeout=10s --retries=5 --start-period=10s CMD pgrep -f 'php-fpm: pool frontend'

ENTRYPOINT ["/entrypoint.sh"]
