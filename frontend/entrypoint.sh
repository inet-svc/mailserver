#!/bin/sh

## Apply templates for postfixadmin config
echo "* Applying postfixadmin config"
if [ ! -z "${POSTFIXADMIN_SETUP_PASSWORD}" ]; then
    export POSTFIXADMIN_SETUP_PASSWORD_HASH=$(php -r "echo password_hash(\"${POSTFIXADMIN_SETUP_PASSWORD}\", PASSWORD_DEFAULT);")
fi
for i in $(find /etc/postfixadmin -type f -name *.j2); do
    FILE_IN=${i}
    FILE_OUT=${i%%.j2}

    echo "  - ${FILE_OUT}"
    j2 -o ${FILE_OUT} ${FILE_IN}
done

## Apply templates for roundcube config
echo "* Applying roundcube config"
for i in $(find /etc/roundcube -type f -name *.j2); do
    FILE_IN=${i}
    FILE_OUT=${i%%.j2}

    echo "  - ${FILE_OUT}"
    j2 -o ${FILE_OUT} ${FILE_IN}
done
echo "* Initializing roundcube database"
/var/www/roundcube/bin/initdb.sh --dir /var/www/roundcube/SQL 2>/dev/null

## Apply templates for z-push config
echo "* Applying z-push config"
for i in $(find /etc/z-push -type f -name *.j2); do
    FILE_IN=${i}
    FILE_OUT=${i%%.j2}

    echo "  - ${FILE_OUT}"
    j2 -o ${FILE_OUT} ${FILE_IN}
done

mkdir -p /var/www/postfixadmin/templates_c
chown www-data.www-data /var/www/postfixadmin/templates_c

mkdir -p /var/www/roundcube/temp /var/www/roundcube/logs
chown www-data.www-data /var/www/roundcube/temp /var/www/roundcube/logs

mkdir -p /var/log/postfixadmin
mkdir -p /var/log/z-push

chown www-data.www-data /var/log/postfixadmin
chown www-data.www-data /var/log/z-push

## Initialize postfixadmin database
echo "* Initializing postfixadmin database"
php /var/www/postfixadmin/public/upgrade.php | sed -e 's/<[^>]*>//g'

## Initialize postfixadmin superadmin
if [ ! -z "${POSTFIXADMIN_ADMIN_EMAIL}" ] && [ ! -z "${POSTFIXADMIN_ADMIN_PASSWORD}" ]; then
    if /var/www/postfixadmin/scripts/postfixadmin-cli admin view ${POSTFIXADMIN_ADMIN_EMAIL} >/dev/null 2>&1; then
        echo "* Postfixadmin master admin account already created"
    else
        echo "* Creating postfixadmin master admin account"
        /var/www/postfixadmin/scripts/postfixadmin-cli admin add ${POSTFIXADMIN_ADMIN_EMAIL} --password "${POSTFIXADMIN_ADMIN_PASSWORD}" --password2 "${POSTFIXADMIN_ADMIN_PASSWORD}" --superadmin --active
    fi
fi

## Disable admin UI if superadmin is configured
if /var/www/postfixadmin/scripts/postfixadmin-cli admin view ${POSTFIXADMIN_ADMIN_EMAIL} >/dev/null 2>&1; then
    export POSTFIXADMIN_INITIALIZED=1
    echo "* Disabling access to postfixadmin setup"
else
    echo "* Postfixadmin setup is available at http(s)://${SERVER_NAME}/${FRONTEND_ADMIN_CONTEXT}/setup.php"
fi

## Apply templates for nginx config
echo "* Applying nginx config"
for i in $(find /etc/nginx -type f -name *.j2); do
    FILE_IN=${i}
    FILE_OUT=${i%%.j2}

    echo "  - ${FILE_OUT}"
    j2 -o ${FILE_OUT} ${FILE_IN}
done

## Create postfixadmin script conf file
cat << EOF >/usr/lib/postfixadmin/scripts/.config
MYSQL_HOST=${MYSQL_HOST}
SABRE_MYSQL_DATABASE=${SABRE_MYSQL_DATABASE}
SABRE_MYSQL_USER=${SABRE_MYSQL_USER}
SABRE_MYSQL_PASSWORD=${SABRE_MYSQL_PASSWORD}
EOF

/usr/bin/memcached -u memcache -d
/usr/sbin/php-fpm7.4
exec /usr/sbin/nginx
