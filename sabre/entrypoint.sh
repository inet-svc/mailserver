#!/bin/sh

## Apply templates for sabre config
echo "* Applying sabre config"
for i in $(find /var/www/sabre -type f -name *.j2); do
    FILE_IN=${i}
    FILE_OUT=${i%%.j2}

    echo "  - ${FILE_OUT}"
    j2 -o ${FILE_OUT} ${FILE_IN}
done

mkdir -p /var/lib/sabre
chown www-data.www-data /var/lib/sabre

/usr/sbin/php-fpm8.1
exec /usr/sbin/nginx
