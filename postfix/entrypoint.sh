#!/bin/bash

## Apply templates for postfix config
echo "* Applying postfix config"
for i in $(find /etc/postfix -type f -name *.j2); do
    FILE_IN=${i}
    FILE_OUT=${i%%.j2}

    echo "  - ${FILE_OUT}"
    j2 -o ${FILE_OUT} ${FILE_IN}
done

## Configure mynetworks
echo "* Configuring postfix myhosts"
MYNETWORKS="127.0.0.0/8 [::ffff:127.0.0.0]/104 [::1]/128"
for i in $(ip -o -f inet ro | awk '!/via/ {print $1}'); do MYNETWORKS+=" ${i}"; done
for i in $(ip -o -f inet6 ro | awk '!/via/ {split($1,ip,"/"); print "["ip[1]"]/" ip[2]}'); do MYNETWORKS+=" ${i}"; done
for i in ${RELAY_NETWORKS}; do MYNETWORKS+=" ${i}"; done
for i in ${MYNETWORKS}; do echo "  + ${i}"; done
postconf -e mynetworks="${MYNETWORKS}"

## Add resolv.conf for chrooted processes
mkdir -p /var/spool/postfix/etc
cp -f /etc/resolv.conf /var/spool/postfix/etc/resolv.conf

## Replace current shell with postfix
exec /usr/sbin/postfix -c /etc/postfix start-fg
